﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class Kostka5 : KostkaABS, INoRotating
    {// čtverec 2x2
        PointXY dilekDoleVlevo = new PointXY();
        PointXY dilekDoleVpravo = new PointXY();
        PointXY dilekNahoreVpravo = new PointXY();
        public Kostka5()
            : base()
        {
            vsechnyDilky.Add(dilekHLAVNI);
            vsechnyDilky.Add(dilekNahoreVpravo);
            vsechnyDilky.Add(dilekDoleVpravo);
            vsechnyDilky.Add(dilekDoleVlevo);
            UvodniNacteniDoPretoceni1();
        }
        public override void ZmenaRadku1()
        {
            UvodniNacteniDoPretoceni1();
        }
        public override void ZmenaRadku2()
        {
            ZmenaRadku1();
        }
        public override void ZmenaRadku3()
        {
            ZmenaRadku1();
        }
        public override void ZmenaRadku4()
        {
            ZmenaRadku1();
        }
        public override void ZmenaSloupce1()
        {
            UvodniNacteniDoPretoceni1();
        }
        public override void ZmenaSloupce2()
        {
            ZmenaSloupce1();
        }
        public override void ZmenaSloupce3()
        {
            ZmenaSloupce1();
        }
        public override void ZmenaSloupce4()
        {
            ZmenaSloupce1();
        }
        public override void PretocDo1()
        {
            UvodniNacteniDoPretoceni1();
        }
        public override void PretocDo2()
        {
            PretocDo1();
        }
        public override void PretocDo3()
        {
            PretocDo1();
        }
        public override void PretocDo4()
        {
            PretocDo1();
        }
        public override void UvodniNacteniDoPretoceni1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekDoleVlevo.Radek = RadekHlavnihoDilku - 1;
            dilekDoleVlevo.Sloupec = SloupecHlavnihoDilku;
            dilekDoleVpravo.Radek = RadekHlavnihoDilku - 1;
            dilekDoleVpravo.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahoreVpravo.Radek = RadekHlavnihoDilku;
            dilekNahoreVpravo.Sloupec = SloupecHlavnihoDilku + 1;
        }
        public override void NasledujiciKostka(char renderOrErase)
        {
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky - 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky - 1);
        }
        public override bool VolnaCestaDo1()
        {
            return true;
        }
        public override bool VolnaCestaDo2()
        {
            return true;
        }
        public override bool VolnaCestaDo3()
        {
            return true;
        }
        public override bool VolnaCestaDo4()
        {
            return true;
        }
    }
}
