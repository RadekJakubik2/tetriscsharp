﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    public abstract class KostkaABS
    {
        private int typOtoceni = 1;
        public const int RadekProVykresleniNasledujiciKostky = 7;
        public const int SloupecProVykresleniNasledujiciKostky = Storage.sloupecPravehoMenu;
        private int radekHlavnihoDilku;
        private int sloupecHlavnihoDilku;
        public PointXY dilekHLAVNI = new PointXY();
        public List<PointXY> vsechnyDilky = new List<PointXY>();
        public List<PointXY> predeslyStavVsechDilku = new List<PointXY>();
        private bool posledniPretoceniZamitnuto = false;
        public KostkaABS()
        {
            radekHlavnihoDilku = -1;       // každá kostka začíná padat z řádku mínus 1, aby nešla hned vidět
            sloupecHlavnihoDilku = 5;      // každá kostka začíná padat z prostředka
        }
        public abstract void ZmenaRadku1();
        public abstract void ZmenaRadku2();
        public abstract void ZmenaRadku3();
        public abstract void ZmenaRadku4();
        public abstract void ZmenaSloupce1();
        public abstract void ZmenaSloupce2();
        public abstract void ZmenaSloupce3();
        public abstract void ZmenaSloupce4();
        public abstract void PretocDo1();
        public abstract void PretocDo2();
        public abstract void PretocDo3();
        public abstract void PretocDo4();
        public abstract void NasledujiciKostka(char renderOrErase); // mrizkou vypisu do konzole, mezerou z ni vymazu
        public abstract void UvodniNacteniDoPretoceni1();
        public abstract bool VolnaCestaDo1();
        public abstract bool VolnaCestaDo2();
        public abstract bool VolnaCestaDo3();
        public abstract bool VolnaCestaDo4();


        public int RadekHlavnihoDilku
        {
            get { return radekHlavnihoDilku; }
            set
            {
                radekHlavnihoDilku = value;
                if (typOtoceni == 1) ZmenaRadku1();
                else if (typOtoceni == 2) ZmenaRadku2();
                else if (typOtoceni == 3) ZmenaRadku3();
                else if (typOtoceni == 4) ZmenaRadku4();
            }
        }
        public int SloupecHlavnihoDilku
        {
            get { return sloupecHlavnihoDilku; }
            set
            {
                sloupecHlavnihoDilku = value;
                if (typOtoceni == 1) ZmenaSloupce1();
                else if (typOtoceni == 2) ZmenaSloupce2();
                else if (typOtoceni == 3) ZmenaSloupce3();
                else if (typOtoceni == 4) ZmenaSloupce4();
            }
        }
        public int PretoceniKostkyDoprava
        {
            get { return typOtoceni; }
            set
            {
                typOtoceni = value;
                if (typOtoceni == 5) typOtoceni = 1;
                else if (typOtoceni == 0) typOtoceni = 4;

                if (typOtoceni == 1) PretocDo1();
                else if (typOtoceni == 2) PretocDo2();
                else if (typOtoceni == 3) PretocDo3();
                else if (typOtoceni == 4) PretocDo4();
                posledniPretoceniZamitnuto = false;
            }
        }

        public void NactiStavPredOtocenim()
        {
            if (Storage.noRotatingType) return;
            for (int i = 0; i < vsechnyDilky.Count; i++)
            {
                predeslyStavVsechDilku[i].Radek = vsechnyDilky[i].Radek;
                predeslyStavVsechDilku[i].Sloupec = vsechnyDilky[i].Sloupec;
            }
        }

        public void VratitPretoceniZpet()
        {
            if (Storage.noRotatingType) return;
            for (int i = 0; i < vsechnyDilky.Count; i++)
            {
                vsechnyDilky[i].Radek = predeslyStavVsechDilku[i].Radek;
                vsechnyDilky[i].Sloupec = predeslyStavVsechDilku[i].Sloupec;
            }
            sloupecHlavnihoDilku = predeslyStavVsechDilku[0].Sloupec;
            radekHlavnihoDilku = predeslyStavVsechDilku[0].Radek; ;
            if (!posledniPretoceniZamitnuto)
                typOtoceni--;
            posledniPretoceniZamitnuto = true;
        }

        public bool ZakazaneSouradniceNeprekazi()
        {
            if (typOtoceni == 1) return VolnaCestaDo2();
            else if (typOtoceni == 2) return VolnaCestaDo3();
            else if (typOtoceni == 3) return VolnaCestaDo4();
            else if (typOtoceni == 4) return VolnaCestaDo1();
            return true;
        }
    }
}
