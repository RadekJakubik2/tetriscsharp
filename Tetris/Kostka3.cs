﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class Kostka3 : KostkaABS
    {//elko doleva
        PointXY dilekNahore1 = new PointXY();
        PointXY dilekNahore2 = new PointXY();
        PointXY dilekVlevo = new PointXY();
        public Kostka3()
            : base()
        {
            vsechnyDilky.Add(dilekHLAVNI);
            vsechnyDilky.Add(dilekVlevo);
            vsechnyDilky.Add(dilekNahore2);
            vsechnyDilky.Add(dilekNahore1);
            foreach (var item in vsechnyDilky)
            {
                predeslyStavVsechDilku.Add(new PointXY(item));
            }
            UvodniNacteniDoPretoceni1();
        }
        public override void ZmenaRadku2()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku - 1;
            dilekNahore2.Radek = RadekHlavnihoDilku - 2;
            dilekVlevo.Radek = RadekHlavnihoDilku;
        }
        public override void ZmenaRadku3()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku - 1;
        }
        public override void ZmenaRadku4()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku + 1;
            dilekNahore2.Radek = RadekHlavnihoDilku + 2;
            dilekVlevo.Radek = RadekHlavnihoDilku;
        }
        public override void ZmenaRadku1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku + 1;
        }
        public override void ZmenaSloupce2()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
        }
        public override void ZmenaSloupce3()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku + 2;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku;
        }
        public override void ZmenaSloupce4()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku + 1;
        }
        public override void ZmenaSloupce1()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku - 2;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku;
        }
        public override void PretocDo2()
        {
            NactiStavPredOtocenim();
            RadekHlavnihoDilku++;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku - 1;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku - 2;
        }
        public override void PretocDo3()
        {
            NactiStavPredOtocenim();
            SloupecHlavnihoDilku--;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku - 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku + 2;
            dilekNahore2.Radek = RadekHlavnihoDilku;
        }
        public override void PretocDo4()
        {
            NactiStavPredOtocenim();
            RadekHlavnihoDilku--;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku + 1;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku + 1;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku + 2;
        }
        public override void PretocDo1()
        {
            NactiStavPredOtocenim();
            SloupecHlavnihoDilku++;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku + 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku - 2;
            dilekNahore2.Radek = RadekHlavnihoDilku;
        }
        public override void UvodniNacteniDoPretoceni1()
        {
            NactiStavPredOtocenim();
            SloupecHlavnihoDilku++; RadekHlavnihoDilku--;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku + 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku - 2;
            dilekNahore2.Radek = RadekHlavnihoDilku;
        }
        public override void NasledujiciKostka(char renderOrErase)
        {
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky + 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky + 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky - 1);
        }
        public override bool VolnaCestaDo1()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku + 2, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo2()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 2, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo3()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku - 2, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo4()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 2, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
    }
}
