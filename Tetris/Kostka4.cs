﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class Kostka4 : KostkaABS
    {   //svislá čtyřka
        PointXY dilekDole = new PointXY();
        PointXY dilekNahore2 = new PointXY();
        PointXY dilekNahore1 = new PointXY();
        public Kostka4()
            : base()
        {
            vsechnyDilky.Add(dilekHLAVNI);
            vsechnyDilky.Add(dilekNahore1);
            vsechnyDilky.Add(dilekNahore2);
            vsechnyDilky.Add(dilekDole);
            foreach (var item in vsechnyDilky)
            {
                predeslyStavVsechDilku.Add(new PointXY(item));
            }
            UvodniNacteniDoPretoceni1();
        }
        public override void ZmenaRadku1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekDole.Radek = RadekHlavnihoDilku;
            dilekDole.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku + 2;
        }
        public override void ZmenaRadku2()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekDole.Radek = RadekHlavnihoDilku + 1;
            dilekDole.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku - 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku - 2;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
        }
        public override void ZmenaRadku3()
        {
            ZmenaRadku1();
        }
        public override void ZmenaRadku4()
        {
            ZmenaRadku2();
        }
        public override void ZmenaSloupce1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekDole.Radek = RadekHlavnihoDilku;
            dilekDole.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku + 2;
        }
        public override void ZmenaSloupce2()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekDole.Radek = RadekHlavnihoDilku + 1;
            dilekDole.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku - 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku - 2;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
        }
        public override void ZmenaSloupce3()
        {
            ZmenaSloupce1();
        }
        public override void ZmenaSloupce4()
        {
            ZmenaSloupce2();
        }
        public override void PretocDo1()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekDole.Radek = RadekHlavnihoDilku;
            dilekDole.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku + 2;
        }

        public override void PretocDo2()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekDole.Radek = RadekHlavnihoDilku + 1;
            dilekDole.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku - 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku - 2;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
        }
        public override void PretocDo3()
        {
            PretocDo1();
        }
        public override void PretocDo4()
        {
            PretocDo2();
        }
        public override void UvodniNacteniDoPretoceni1()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku - 1;
            dilekDole.Radek = RadekHlavnihoDilku;
            dilekDole.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku + 2;
        }
        public override void NasledujiciKostka(char renderOrErase)
        {
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky - 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky - 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 2, RadekProVykresleniNasledujiciKostky - 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 3, RadekProVykresleniNasledujiciKostky - 1);
        }
        public override bool VolnaCestaDo1()
        {// sem to chodí již s posunutými sloupci!!!
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku - 2, EnumStavPole.Obsazeno))
                return false;

            if (SloupecHlavnihoDilku + 2 > Storage.indexZpravaPrvniVolnySloupec)
                return true; 
            else if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 2, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo2()
        {
            return VolnaCestaDo1();
        }
        public override bool VolnaCestaDo3()
        {
            return VolnaCestaDo1();
        }
        public override bool VolnaCestaDo4()
        {
            return VolnaCestaDo1();
        }
    }
}
