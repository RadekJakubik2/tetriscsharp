﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{  
    public class PointXY
    {
        public PointXY()
        {

        }
        public PointXY(PointXY obj)
        {
            Radek = obj.Radek;
            Sloupec = obj.Sloupec;
        }
        public int Sloupec { get; set; }
        public int Radek { get; set; }
    }
}
