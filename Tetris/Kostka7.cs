﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class Kostka7 : KostkaABS
    {//esko doprava
        PointXY dilekNahore = new PointXY();
        PointXY dilekDoleVlevo = new PointXY();
        PointXY dilekNahoreVpravo = new PointXY();
        public Kostka7()
            : base()
        {
            vsechnyDilky.Add(dilekHLAVNI);
            vsechnyDilky.Add(dilekNahoreVpravo);
            vsechnyDilky.Add(dilekDoleVlevo);
            vsechnyDilky.Add(dilekNahore);
            foreach (var item in vsechnyDilky)
            {
                predeslyStavVsechDilku.Add(new PointXY(item));
            }
            UvodniNacteniDoPretoceni1();
        }
        public override void ZmenaRadku1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekDoleVlevo.Radek = RadekHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku - 1;
            dilekNahoreVpravo.Radek = RadekHlavnihoDilku - 1;
        }
        public override void ZmenaRadku2()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekDoleVlevo.Radek = RadekHlavnihoDilku + 1;
            dilekNahore.Radek = RadekHlavnihoDilku;
            dilekNahoreVpravo.Radek = RadekHlavnihoDilku - 1;
        }
        public override void ZmenaRadku3()
        {
            ZmenaRadku1();
        }
        public override void ZmenaRadku4()
        {
            ZmenaRadku2();
        }
        public override void ZmenaSloupce1()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekDoleVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahoreVpravo.Sloupec = SloupecHlavnihoDilku + 1;
        }
        public override void ZmenaSloupce2()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku - 1;
            dilekDoleVlevo.Sloupec = SloupecHlavnihoDilku;
            dilekNahoreVpravo.Sloupec = SloupecHlavnihoDilku - 1;
        }
        public override void ZmenaSloupce3()
        {
            ZmenaSloupce1();
        }
        public override void ZmenaSloupce4()
        {
            ZmenaSloupce2();
        }
        public override void PretocDo1()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku - 1;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekNahoreVpravo.Radek = RadekHlavnihoDilku - 1;
            dilekNahoreVpravo.Sloupec = SloupecHlavnihoDilku + 1;
            dilekDoleVlevo.Radek = RadekHlavnihoDilku;
            dilekDoleVlevo.Sloupec = SloupecHlavnihoDilku - 1;
        }
        public override void PretocDo2()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahoreVpravo.Radek = RadekHlavnihoDilku - 1;
            dilekNahoreVpravo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekDoleVlevo.Radek = RadekHlavnihoDilku + 1;
            dilekDoleVlevo.Sloupec = SloupecHlavnihoDilku;
        }
        public override void PretocDo3()
        {
            PretocDo1();
        }
        public override void PretocDo4()
        {
            PretocDo2();
        }
        public override void UvodniNacteniDoPretoceni1()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku - 1;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekNahoreVpravo.Radek = RadekHlavnihoDilku - 1;
            dilekNahoreVpravo.Sloupec = SloupecHlavnihoDilku + 1;
            dilekDoleVlevo.Radek = RadekHlavnihoDilku;
            dilekDoleVlevo.Sloupec = SloupecHlavnihoDilku - 1;
        }
        public override void NasledujiciKostka(char renderOrErase)
        {
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky - 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 2, RadekProVykresleniNasledujiciKostky - 1);
        }
        public override bool VolnaCestaDo1()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo2()
        {
            return VolnaCestaDo1();
        }
        public override bool VolnaCestaDo3()
        {
            return VolnaCestaDo1();
        }
        public override bool VolnaCestaDo4()
        {
            return VolnaCestaDo2();
        }
    }
}
