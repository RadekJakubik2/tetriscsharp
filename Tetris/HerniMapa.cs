﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Tetris
{
    public enum EnumStavPole
    {
        Prazdne, Obsazeno, RadaHotova, PravyOkraj, LevyOkraj, DolniOkraj, HorniOkraj
    }
    class HerniMapa
    {
        List<int> radkyDosednuteKostky = new List<int>();
        int skore = 0;
        int rychlostHry = 1;
        KostkaABS pristiKostka;
        KostkaABS aktualniKostka;
        Timer timer = new Timer();

        public HerniMapa()
        {
            Console.CursorVisible = false;
            NastavRychlostHry();
            timer.Elapsed += timer_Elapsed;
            VykresliUvodniPopisky();
            VykresliOhraniceniMapy();
            VygenerovatNovouKostku();   // prvni kostku
            VygenerovatNovouKostku();   // druhou kostku
            Storage.VykresliNaMistoKostky('#', aktualniKostka);
            HerniSmycka();
        }
        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PosunKostky(ConsoleKey.DownArrow);
        }
        private void HerniSmycka()
        {
            timer.Start();
            while (true)
            {
                ConsoleKeyInfo stisknutaKlavesa = Console.ReadKey(true);
                PosunKostky(stisknutaKlavesa.Key);
            }
        }

        private void UkonciHru()
        {
            timer.Dispose();
            Storage.OznamUkonceniHry(aktualniKostka);
            System.Threading.Thread.Sleep(5000);
            Environment.Exit(0);
        }

        private void KontrolaZdaJeRada()
        {
            List<int> radkySRadami = new List<int>();
            foreach (int item in radkyDosednuteKostky)
            {
                for (int sloupec = 1; sloupec < Storage.sloupceCelkem - 1; sloupec++)
                {
                    if (Storage.mapaEnum[sloupec, item] != EnumStavPole.Obsazeno)
                        break;
                    else if (sloupec == Storage.sloupceCelkem - 2)
                        radkySRadami.Add(item);
                }
            }
            if (radkySRadami.Count > 0)
            {
                OznacitRadkySRadou(radkySRadami);
                OdstranitRadyZMapy();
                VypocitejSkoreZaRady(radkySRadami.Count);
                NastavRychlostHry();
                PrekresliKostkyVKonzoli();
            }
        }
        private void OznacitRadkySRadou(List<int> radkySRadami)
        {
            foreach (int item in radkySRadami)
                for (int sloupec = 1; sloupec < Storage.sloupceCelkem - 2; sloupec++)
                    Storage.mapaEnum[sloupec, item] = EnumStavPole.RadaHotova;
        }
        private void OdstranitRadyZMapy()
        {
            bool tedBylaRada = false;
            int pocetNalezenychRad = 0;
            for (int radekKAMseKopiruje = (Storage.radkyCelkem - 2); radekKAMseKopiruje > 0; radekKAMseKopiruje--)
            {
                int radekODKUDseKopiruje = radekKAMseKopiruje - pocetNalezenychRad;  // může se dostat do mínusu, pozor na to
                for (int aktualniSloupec = 1; aktualniSloupec < (Storage.sloupceCelkem - 1); aktualniSloupec++)
                {
                    if (NarazilJsemNaRadu(radekODKUDseKopiruje, aktualniSloupec))
                    {
                        tedBylaRada = true;
                        pocetNalezenychRad++;
                    }
                    if (radekODKUDseKopiruje < 1) // musí být první podmínkou tady
                        Storage.mapaEnum[aktualniSloupec, radekKAMseKopiruje] = EnumStavPole.Prazdne;
                    else if (tedBylaRada == true) // je řada
                        Storage.mapaEnum[aktualniSloupec, radekKAMseKopiruje] = EnumStavPole.Prazdne;
                    else if (tedBylaRada == false) // není řada
                        Storage.mapaEnum[aktualniSloupec, radekKAMseKopiruje] = Storage.mapaEnum[aktualniSloupec, radekODKUDseKopiruje];
                }
                if (tedBylaRada)
                {
                    radekKAMseKopiruje++;
                    tedBylaRada = false;
                }
            }
        }

        private bool NarazilJsemNaRadu(int radekODKUDseKopiruje, int aktualniSloupec)
        {
            if (radekODKUDseKopiruje < 0)
                radekODKUDseKopiruje = 0;
            if (aktualniSloupec == 1 && Storage.PoleJeObsazeno(aktualniSloupec, radekODKUDseKopiruje, EnumStavPole.RadaHotova))
                return true;
            return false;
        }
        private void VypocitejSkoreZaRady(int pocetNalezenychRad)
        {
            if (pocetNalezenychRad == 1) skore += 10;
            else if (pocetNalezenychRad == 2) skore += 30;
            else if (pocetNalezenychRad == 3) skore += 60;
            else if (pocetNalezenychRad == 4) skore += 100;
        }
        private void NastavRychlostHry()
        {
            if (skore < 10)
            {
                rychlostHry = 1;
                timer.Interval = 1000;
            }
            else if (skore >= 10 && skore < 20)
            {
                rychlostHry = 2;
                timer.Interval = 300;
            }
            else
            {
                rychlostHry = 3;
                timer.Interval = 100;
            }
        }
        private void PrekresliKostkyVKonzoli()
        {
            for (int radky = (Storage.radkyCelkem - 2); radky > 0; radky--)
            {
                for (int sloupce = Storage.indexZlevaPrvniVolnySloupec; sloupce <= Storage.indexZpravaPrvniVolnySloupec; sloupce++)
                {
                    if (Storage.mapaEnum[sloupce, radky] == EnumStavPole.Prazdne)
                    {
                        Storage.VypisDoKonzole(' ', sloupce, radky, ConsoleColor.Cyan);
                    }
                    else if (Storage.mapaEnum[sloupce, radky] == EnumStavPole.Obsazeno)
                    {
                        Storage.VypisDoKonzole('#', sloupce, radky, ConsoleColor.Cyan);
                    }
                }
            }
            Console.ResetColor();
        }
        private bool NastalKonecHry()
        {
            int radekPriNemzBudeKonecHry = 0;
            for (int i = Storage.indexZlevaPrvniVolnySloupec; i <= Storage.indexZpravaPrvniVolnySloupec; i++)
                if (Storage.PoleJeObsazeno(i, radekPriNemzBudeKonecHry, EnumStavPole.Obsazeno))
                    UkonciHru();
            return false;
        }
        private void VykresliOhraniceniMapy()
        {
            for (int sloupec = 0; sloupec < Storage.sloupceCelkem; sloupec++)
            {
                for (int radek = 0; radek < Storage.radkyCelkem; radek++)
                {
                    if (sloupec == (Storage.sloupceCelkem - 1))
                    {
                        Storage.mapaEnum[sloupec, radek] = EnumStavPole.PravyOkraj;
                        Storage.VypisDoKonzole('#', sloupec, radek);
                    }
                    else if (sloupec == 0)
                    {
                        Storage.mapaEnum[sloupec, radek] = EnumStavPole.LevyOkraj;
                        Storage.VypisDoKonzole('#', sloupec, radek);
                    }
                    else if (radek == Storage.radkyCelkem - 1)
                    {
                        Storage.mapaEnum[sloupec, radek] = EnumStavPole.DolniOkraj;
                        Storage.VypisDoKonzole('#', sloupec, radek);
                    }
                    else if (radek == 0)
                    {
                        Storage.mapaEnum[sloupec, radek] = EnumStavPole.HorniOkraj;
                        Storage.VypisDoKonzole('#', sloupec, radek);
                    }
                    else
                        Storage.mapaEnum[sloupec, radek] = EnumStavPole.Prazdne;
                }
            }
        }

        object key = new object();
        private void PosunKostky(ConsoleKey stisknutaKlavesa)
        { /* vstupuji do ní na dvou místech i dvou různých vláknech (timer a běžná metoda). Proto stisk klávesy může kolidovat s timerem
            a v konzoli může např. zůstat nepřekreslený hash. Proto je zde nutný lock. JDE TO VYŘEŠIT I JINAK? (Farragher notes)*/
            lock (key)
            {
                Storage.VykresliNaMistoKostky(' ', aktualniKostka);
                if (stisknutaKlavesa == ConsoleKey.DownArrow)
                {
                    if (!DownMoveAllowed())
                    {
                        DosednutiKostky();
                        NactiDosednutouKostku();
                        KontrolaZdaJeRada();
                        if (!NastalKonecHry())
                            VygenerovatNovouKostku();
                    }
                    aktualniKostka.RadekHlavnihoDilku++;
                }
                else if (stisknutaKlavesa == ConsoleKey.LeftArrow)
                {
                    if (MoveToLeftAllowed())
                        aktualniKostka.SloupecHlavnihoDilku--;
                }
                else if (stisknutaKlavesa == ConsoleKey.RightArrow)
                {
                    if (MoveToRightAllowed())
                        aktualniKostka.SloupecHlavnihoDilku++;
                }
                else if (stisknutaKlavesa == ConsoleKey.UpArrow)
                {
                    UpArrowMethod();
                }
                Storage.VykresliNaMistoKostky('#', aktualniKostka);
            }
        }

        private void UpArrowMethod()
        {
            if (Storage.noRotatingType || !aktualniKostka.ZakazaneSouradniceNeprekazi()) return;
            else if (OtestovatKoliziSeSpadlymiKostkami()) return;
            else if (OkrajVyhovuje(EnumStavPole.PravyOkraj) != 0) return;
            else if (OkrajVyhovuje(EnumStavPole.LevyOkraj) != 0) return;
            aktualniKostka.PretoceniKostkyDoprava++;
        }

        private bool OtestovatKoliziSeSpadlymiKostkami()
        {
            bool result = false;
            aktualniKostka.PretoceniKostkyDoprava++;
            if (KolizeSLezicimiKostkami())
                result = true;
            aktualniKostka.VratitPretoceniZpet();
            return result;
        }

        private int OkrajVyhovuje(EnumStavPole typOhraniceni)
        {
            aktualniKostka.PretoceniKostkyDoprava++;
            int predeslyPosunSloupcu = 0; int novyPosunSloupcu = 0;
            do
            {
                predeslyPosunSloupcu = novyPosunSloupcu;
                novyPosunSloupcu = KontrolaSloupceSMoznymPosunem(novyPosunSloupcu, typOhraniceni);
            } while (novyPosunSloupcu != predeslyPosunSloupcu);
            // mám 1x otočenou kostku s odsunem 0-2 sloupce.

            if (novyPosunSloupcu == 0)
            {
                aktualniKostka.VratitPretoceniZpet();
            }
            else
            {
                if (KolizeSLezicimiKostkami() || ZakazaneSouradnicePrekazely(novyPosunSloupcu))
                {
                    aktualniKostka.VratitPretoceniZpet();
                    novyPosunSloupcu = int.MinValue; // okraj nevyhovuje (už netřeba zkoumat další okraj)
                }
                //VŠE OK, NECHAT KOSTKU OTOČENOU JAK JE (příp. zde vrátím zpět posun sloupců ze ZakazSouradnicePrekazely)
            }
            return novyPosunSloupcu;
        }

        private bool KolizeSLezicimiKostkami()
        {// nekoliduje od kraje již odsunutá a otočená kostka s Obsazeno (spadlými kostkami)?            
            foreach (var item in aktualniKostka.vsechnyDilky)
            {
                if (item.Sloupec == Storage.sloupceCelkem) continue;
                if (Storage.PoleJeObsazeno(item.Sloupec, item.Radek, EnumStavPole.Obsazeno) ||
                    Storage.PoleJeObsazeno(item.Sloupec, item.Radek, EnumStavPole.DolniOkraj) ||
                    aktualniKostka.RadekHlavnihoDilku <= (-1))
                    return true;
            }
            return false;
        }

        private bool ZakazaneSouradnicePrekazely(int novyPosunSloupcu)
        { // do prázdna od kraje již odsunuté kostce nesmělo v otočení do této polohy nic bránit. Nutné vrátit zpět a znovu otočit k prověření tohoto.
            //1) zamítnu otočení (vrátím kostku na úplně původní hodnotu před otočením, včetně původních sloupců)
            aktualniKostka.VratitPretoceniZpet();
            //2) kostku posunu o patřičný počet sloupců tam kde byla (ale zatím nepřetáčím)
            if (!VolnaCestaKPosuvu(novyPosunSloupcu * (-1)))
                return true;
            //3) prověřím zakázané souřadnice
            else if (aktualniKostka.ZakazaneSouradniceNeprekazi())
                // 4) můžu bez obav otočit!
                aktualniKostka.PretoceniKostkyDoprava++;
            else
                return true;
            return false;
        }
        private bool VolnaCestaKPosuvu(int novyPosunSloupcu)
        {
            int absValue = Math.Abs(novyPosunSloupcu);
            int sign = (novyPosunSloupcu > 0) ? 1 : -1;
            for (int i = 1; i <= absValue; i++)
            {
                aktualniKostka.SloupecHlavnihoDilku += (1 * sign);
                if (KolizeSLezicimiKostkami())
                    return false;
            }
            return true;
        }
        private int KontrolaSloupceSMoznymPosunem(int novyPosunSloupcu, EnumStavPole typOhraniceni)
        {
            foreach (var item in aktualniKostka.vsechnyDilky)
            {
                if (item.Sloupec == Storage.sloupceCelkem) continue; // mě zajímá jen samotná kolize s ohraničením
                if (Storage.PoleJeObsazeno(item.Sloupec, item.Radek, typOhraniceni))
                {
                    if (typOhraniceni == EnumStavPole.LevyOkraj)
                        return PosunLevehoSloupce(novyPosunSloupcu);
                    else if (typOhraniceni == EnumStavPole.PravyOkraj)
                        return PosunPravehoSloupce(novyPosunSloupcu);
                }
            }
            return novyPosunSloupcu;
        }
        private int PosunLevehoSloupce(int novyPosunSloupcu)
        {
            aktualniKostka.SloupecHlavnihoDilku++;
            return --novyPosunSloupcu;
        }
        private int PosunPravehoSloupce(int novyPosunSloupcu)
        {
            aktualniKostka.SloupecHlavnihoDilku--;
            return ++novyPosunSloupcu;
        }

        private bool DownMoveAllowed()
        {
            foreach (PointXY item in aktualniKostka.vsechnyDilky)
            {
                if (Storage.PoleJeObsazeno(item.Sloupec, item.Radek + 1, EnumStavPole.Obsazeno) ||
                    Storage.PoleJeObsazeno(item.Sloupec, item.Radek + 1, EnumStavPole.DolniOkraj))
                    return false;
            }
            return true;
        }

        private bool MoveToLeftAllowed()
        {
            foreach (PointXY item in aktualniKostka.vsechnyDilky)
                if (Storage.PoleJeObsazeno(item.Sloupec - 1, item.Radek, EnumStavPole.Obsazeno) ||
                    Storage.PoleJeObsazeno(item.Sloupec - 1, item.Radek, EnumStavPole.LevyOkraj) ||
                    item.Sloupec - 1 < Storage.indexZlevaPrvniVolnySloupec)
                    return false;
            return true;
        }

        private bool MoveToRightAllowed()
        {
            foreach (PointXY item in aktualniKostka.vsechnyDilky)
                if (Storage.PoleJeObsazeno(item.Sloupec + 1, item.Radek, EnumStavPole.Obsazeno) ||
                    Storage.PoleJeObsazeno(item.Sloupec + 1, item.Radek, EnumStavPole.PravyOkraj) ||
                    item.Sloupec + 1 > Storage.indexZpravaPrvniVolnySloupec)
                    return false;
            return true;
        }

        private void DosednutiKostky()
        {
            foreach (PointXY item in aktualniKostka.vsechnyDilky)
            {
                if (item.Radek < 0)
                    continue;
                Storage.mapaEnum[item.Sloupec, item.Radek] = EnumStavPole.Obsazeno;
                if (item.Radek != 0)
                {
                    Storage.VypisDoKonzole('#', item.Sloupec, item.Radek, ConsoleColor.Cyan);
                }
            }
            Console.ResetColor();
        }
        private void NactiDosednutouKostku()
        {
            radkyDosednuteKostky.Clear();
            foreach (PointXY item in aktualniKostka.vsechnyDilky)
                if (item.Radek >= 0)
                    radkyDosednuteKostky.Add(item.Radek);
            radkyDosednuteKostky = radkyDosednuteKostky.Distinct().ToList();
        }

        private void PrekresliPravyPanel()
        {
            ZobrazPristiKostku();
            ZobrazSkore();
            ZobrazRychlostHry();
        }
        private void ZobrazPristiKostku()
        {
            aktualniKostka.NasledujiciKostka(' ');
            pristiKostka.NasledujiciKostka('#');
        }
        private void ZobrazSkore()
        {
            Storage.VypisDoKonzole(skore.ToString(), Storage.sloupecPravehoMenu, 12);
        }
        private void ZobrazRychlostHry()
        {
            Storage.VypisDoKonzole(rychlostHry.ToString(), Storage.sloupecPravehoMenu, 17);
        }
        private void VykresliUvodniPopisky()
        {
            Storage.VypisDoKonzole("Následující kostka:", Storage.sloupecPravehoMenu, 4);
            Storage.VypisDoKonzole("Skóre:", Storage.sloupecPravehoMenu, 11);
            Storage.VypisDoKonzole("Rychlost hry:", Storage.sloupecPravehoMenu, 16);
        }
        private void VygenerovatNovouKostku()
        {
            Random random = new Random();
            aktualniKostka = pristiKostka;
            if (aktualniKostka is INoRotating) Storage.noRotatingType = true;
            else Storage.noRotatingType = false;

            switch (random.Next(1, 9))            
            //switch (random.Next(4, 5))
            {
                case 1: // kříž
                    pristiKostka = new Kostka1();
                    break;
                case 2: // elko doprava
                    pristiKostka = new Kostka2();
                    break;
                case 3: // elko doleva
                    pristiKostka = new Kostka3();
                    break;
                case 4: // čtyřka
                    pristiKostka = new Kostka4();
                    break;
                case 5: // čtverec
                    pristiKostka = new Kostka5();
                    break;
                case 6: // esko doleva
                    pristiKostka = new Kostka6();
                    break;
                case 7: // esko doprava
                    pristiKostka = new Kostka7();
                    break;
                case 8: // stříška
                    pristiKostka = new Kostka8();
                    break;
                default:
                    break;
            }
            if (aktualniKostka != null)
                PrekresliPravyPanel();
        }
    }
}
