﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    public static class Storage
    {        
        public const int sloupceCelkem = 12;                                    // včetně hranic
        public const int radkyCelkem = 24;                                      // včetně hranic
        public const int indexZlevaPrvniVolnySloupec = 1;                       // nula je # hranice
        public const int indexZpravaPrvniVolnySloupec = sloupceCelkem - 2;      // to mínus je # hranice napravo a ještě index 0        
        public const int sloupecPravehoMenu = sloupceCelkem + 4;
        public static EnumStavPole[,] mapaEnum = new EnumStavPole[sloupceCelkem, radkyCelkem];
        public static bool noRotatingType = false;

        public static void VypisDoKonzole(string str, int sloupec, int radek)
        {
            if (radek < 0) return;
            Console.SetCursorPosition(sloupec, radek);
            Console.Write(str);
        }

        public static void VypisDoKonzole(char chr, int sloupec, int radek)
        {
            if (radek < 0) return;
            Console.SetCursorPosition(sloupec, radek);
            Console.Write(chr);
        }

        public static void VypisDoKonzole(char chr, int sloupec, int radek, ConsoleColor color)
        {
            if (radek < 0) return;
            Console.SetCursorPosition(sloupec, radek);
            Console.ForegroundColor = color;
            Console.Write(chr);
        }

        public static void OznamUkonceniHry(KostkaABS aktualniKostka)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            VykresliNaMistoKostky('#', aktualniKostka);
            Console.SetCursorPosition(1, 10);
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.Write(" KONEC HRY");
            Console.ResetColor();
        }

        public static bool PoleJeObsazeno(int sloupec, int radek, EnumStavPole enumHodnotaObsazenosti)
        {
            if (radek < 0) // řádek zpočátku padá ze záporných řádků
                return false;
            else
                return mapaEnum[sloupec, radek] == enumHodnotaObsazenosti;
        }

        public static void VykresliNaMistoKostky(char chr, KostkaABS kostka)
        {
            foreach (PointXY item in kostka.vsechnyDilky)
            {
                if (item.Radek > 0)
                {
                    VypisDoKonzole(chr, item.Sloupec, item.Radek);
                }
            }
        }
    }
}
