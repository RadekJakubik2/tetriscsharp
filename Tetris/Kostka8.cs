﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class Kostka8 : KostkaABS
    {// stříška
        PointXY dilekVlevo = new PointXY();
        PointXY dilekNahore = new PointXY();
        PointXY dilekVpravo = new PointXY();
        public Kostka8()
            : base()
        {
            vsechnyDilky.Add(dilekHLAVNI);
            vsechnyDilky.Add(dilekVpravo);
            vsechnyDilky.Add(dilekNahore);
            vsechnyDilky.Add(dilekVlevo);
            foreach (var item in vsechnyDilky)
            {
                predeslyStavVsechDilku.Add(new PointXY(item));
            }
            UvodniNacteniDoPretoceni1();
        }
        public override void ZmenaRadku1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku - 1;
        }
        public override void ZmenaRadku2()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku - 1;
            dilekVpravo.Radek = RadekHlavnihoDilku + 1;
            dilekNahore.Radek = RadekHlavnihoDilku;
        }
        public override void ZmenaRadku3()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku + 1;
        }
        public override void ZmenaRadku4()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku - 1;
            dilekVpravo.Radek = RadekHlavnihoDilku + 1;
            dilekNahore.Radek = RadekHlavnihoDilku;
        }
        public override void ZmenaSloupce1()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
        }
        public override void ZmenaSloupce2()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku + 1;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku;
        }
        public override void ZmenaSloupce3()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
        }
        public override void ZmenaSloupce4()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku - 1;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku;
        }
        public override void PretocDo1()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore.Radek = RadekHlavnihoDilku - 1;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
        }
        public override void PretocDo2()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore.Radek = RadekHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku - 1;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku + 1;
        }
        public override void PretocDo3()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku + 1;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
            dilekVpravo.Radek = RadekHlavnihoDilku;
        }
        public override void PretocDo4()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekNahore.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore.Radek = RadekHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku - 1;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku + 1;
        }
        public override void UvodniNacteniDoPretoceni1()
        {
            NactiStavPredOtocenim();
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore.Radek = RadekHlavnihoDilku - 1;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
        }
        public override void NasledujiciKostka(char renderOrErase)
        {
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky - 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 2, RadekProVykresleniNasledujiciKostky);
        }

        public override bool VolnaCestaDo1()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo2()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                 Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                 Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo3()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                 Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                 Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo4()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                 Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno) ||
                 Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
    }
}
