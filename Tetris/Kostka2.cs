﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class Kostka2 : KostkaABS
    {   // elko doprava
        PointXY dilekNahore1 = new PointXY();
        PointXY dilekNahore2 = new PointXY();
        PointXY dilekVpravo = new PointXY();
        public Kostka2()
            : base()
        {
            vsechnyDilky.Add(dilekHLAVNI);
            vsechnyDilky.Add(dilekNahore1);
            vsechnyDilky.Add(dilekNahore2);
            vsechnyDilky.Add(dilekVpravo);
            foreach (var item in vsechnyDilky)
            {
                predeslyStavVsechDilku.Add(new PointXY(item));
            }
            UvodniNacteniDoPretoceni1();
        }
        public override void ZmenaRadku4()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku - 1;
            dilekNahore2.Radek = RadekHlavnihoDilku - 2;
        }
        public override void ZmenaRadku1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku + 1;
        }
        public override void ZmenaRadku2()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku + 1;
            dilekNahore2.Radek = RadekHlavnihoDilku + 2;
        }
        public override void ZmenaRadku3()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku - 1;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku;
        }
        public override void ZmenaSloupce4()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
        }
        public override void ZmenaSloupce1()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku + 2;
        }
        public override void ZmenaSloupce2()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
        }
        public override void ZmenaSloupce3()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku - 2;
        }
        public override void PretocDo4()
        {
            NactiStavPredOtocenim();
            SloupecHlavnihoDilku--;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore1.Radek = RadekHlavnihoDilku - 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku - 2;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
        }
        public override void PretocDo1()
        {
            NactiStavPredOtocenim();
            RadekHlavnihoDilku--;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku + 1;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku + 2;
        }
        public override void PretocDo2()
        {
            NactiStavPredOtocenim();
            SloupecHlavnihoDilku++;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore1.Radek = RadekHlavnihoDilku + 1;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku;
            dilekNahore2.Radek = RadekHlavnihoDilku + 2;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku;
        }
        public override void PretocDo3()
        {
            NactiStavPredOtocenim();
            RadekHlavnihoDilku++;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku - 1;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku - 2;
        }
        public override void UvodniNacteniDoPretoceni1()
        {
            NactiStavPredOtocenim();
            RadekHlavnihoDilku--;
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Radek = RadekHlavnihoDilku + 1;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku;
            dilekNahore1.Radek = RadekHlavnihoDilku;
            dilekNahore1.Sloupec = SloupecHlavnihoDilku + 1;
            dilekNahore2.Radek = RadekHlavnihoDilku;
            dilekNahore2.Sloupec = SloupecHlavnihoDilku + 2;
        }
        public override void NasledujiciKostka(char renderOrErase)
        {
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky + 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky + 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky - 1);
        }

        public override bool VolnaCestaDo1()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 1, RadekHlavnihoDilku - 2, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 2, RadekHlavnihoDilku - 2, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo2()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 2, RadekHlavnihoDilku + 1, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku + 2, RadekHlavnihoDilku + 2, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo3()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 1, RadekHlavnihoDilku + 2, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 2, RadekHlavnihoDilku + 2, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
        public override bool VolnaCestaDo4()
        {
            if (Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 2, RadekHlavnihoDilku - 1, EnumStavPole.Obsazeno) ||
                  Storage.PoleJeObsazeno(SloupecHlavnihoDilku - 2, RadekHlavnihoDilku - 2, EnumStavPole.Obsazeno))
                return false;
            return true;
        }
    }
}
