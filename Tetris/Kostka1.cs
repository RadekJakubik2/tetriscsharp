﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Tetris
{
    class Kostka1 : KostkaABS, INoRotating
    {   // kříž
        PointXY dilekVlevo = new PointXY();
        PointXY dilekNahore = new PointXY();
        PointXY dilekVpravo = new PointXY();
        PointXY dilekDole = new PointXY();

        public Kostka1()
            : base()
        {
            vsechnyDilky.Add(dilekHLAVNI);
            vsechnyDilky.Add(dilekVlevo);
            vsechnyDilky.Add(dilekNahore);
            vsechnyDilky.Add(dilekVpravo);
            vsechnyDilky.Add(dilekDole);
            UvodniNacteniDoPretoceni1();
        }

        public override void ZmenaRadku1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku - 1;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekDole.Radek = RadekHlavnihoDilku + 1;
        }
        public override void ZmenaRadku2()
        {
            ZmenaRadku1();
        }
        public override void ZmenaRadku3()
        {
            ZmenaRadku1();
        }
        public override void ZmenaRadku4()
        {
            ZmenaRadku1();
        }
        public override void ZmenaSloupce1()
        {
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
            dilekDole.Sloupec = SloupecHlavnihoDilku;
        }
        public override void ZmenaSloupce2()
        {
            ZmenaSloupce1();
        }
        public override void ZmenaSloupce3()
        {
            ZmenaSloupce1();
        }
        public override void ZmenaSloupce4()
        {
            ZmenaSloupce1();
        }
        public override void PretocDo1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekDole.Radek = RadekHlavnihoDilku + 1;
            dilekDole.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku - 1;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
        }
        public override void PretocDo2()
        {
            PretocDo1();
        }
        public override void PretocDo3()
        {
            PretocDo1();
        }
        public override void PretocDo4()
        {
            PretocDo1();
        }
        public override void UvodniNacteniDoPretoceni1()
        {
            dilekHLAVNI.Radek = RadekHlavnihoDilku;
            dilekHLAVNI.Sloupec = SloupecHlavnihoDilku;
            dilekDole.Radek = RadekHlavnihoDilku + 1;
            dilekDole.Sloupec = SloupecHlavnihoDilku;
            dilekNahore.Radek = RadekHlavnihoDilku - 1;
            dilekNahore.Sloupec = SloupecHlavnihoDilku;
            dilekVlevo.Radek = RadekHlavnihoDilku;
            dilekVlevo.Sloupec = SloupecHlavnihoDilku - 1;
            dilekVpravo.Radek = RadekHlavnihoDilku;
            dilekVpravo.Sloupec = SloupecHlavnihoDilku + 1;
        }
        public override void NasledujiciKostka(char renderOrErase)
        {
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky + 1);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky + 1, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky - 1, RadekProVykresleniNasledujiciKostky);
            Storage.VypisDoKonzole(renderOrErase, SloupecProVykresleniNasledujiciKostky, RadekProVykresleniNasledujiciKostky - 1);
        }

        public override bool VolnaCestaDo1()
        {
            return true;
        }
        public override bool VolnaCestaDo2()
        {
            return true;
        }
        public override bool VolnaCestaDo3()
        {
            return true;
        }
        public override bool VolnaCestaDo4()
        {
            return true;
        }
    }
}
